<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculoLavaJatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('veiculo_lava_jatos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('veiculo_id')->unsigned();                               
            $table->char('status', 2)->comment('P = Pendende, E = Em andamento, C = Concluido');            
            $table->string('observacoes');
            $table->dateTime('dt_inicio');
            $table->dateTime('dt_fim');
            $table->float('total', 8, 2);            
            $table->float('desconto', 8, 2);            
            $table->timestamps();
        });

        Schema::table('veiculo_lava_jatos', function($table) {
            $table->foreign('veiculo_id')->references('id')->on('veiculos');            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculo_lava_jatos');
    }
}
