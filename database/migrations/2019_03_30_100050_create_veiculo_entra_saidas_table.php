<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculoEntraSaidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculo_entra_saidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('veiculo_lava_jato_id')->unsigned();            
            $table->bigInteger('funcionario_id')->unsigned();            
            $table->char('tipo', 2)->comment('E = Entrada , S = Saida');
            $table->bigInteger('km');
            $table->string('combustivel');
            $table->dateTime('dt_entradasaida');
            $table->timestamps();
        });

        Schema::table('veiculo_entra_saidas', function($table) {
            $table->foreign('veiculo_lava_jato_id')->references('id')->on('veiculo_lava_jatos');
            $table->foreign('funcionario_id')->references('id')->on('funcionarios');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculo_entra_saidas');
    }
}
