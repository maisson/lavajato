<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculoServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {      
        Schema::create('veiculo_servicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('veiculo_lava_jato_id')->unsigned();            
            $table->bigInteger('funcionario_id')->unsigned();            
            $table->bigInteger('servico_id')->unsigned();                                
            $table->string('observacoes');
            $table->float('valor', 8, 2);
            $table->dateTime('dt_inicio');
            $table->dateTime('dt_fim');
            $table->timestamps();
        });

        Schema::table('veiculo_servicos', function($table) {
            $table->foreign('veiculo_lava_jato_id')->references('id')->on('veiculo_lava_jatos');
            $table->foreign('funcionario_id')->references('id')->on('funcionarios');
            $table->foreign('servico_id')->references('id')->on('servicos');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculo_servicos');
    }
}
