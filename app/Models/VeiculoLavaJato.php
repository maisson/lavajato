<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;


class VeiculoLavaJato extends Model
{
    protected $fillable = ['id','veiculo_id','status', 'observacoes', 'dt_inicio', 'dt_fim', 'total', 'desconto']; 
    
    public function veiculo()
    {
        return $this->belongsTo(Veiculo::class);
    }  

    public function veiculoServico()
    {
        return $this->belongsTo(VeiculoServico::class);
    }

    public function veiculoEntradaSaida()
    {
        return $this->belongsToMany(VeiculoEntraSaida::class);
    }

    public function getResults( $data, $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['id']) )
                $query->where( 'id', $data['id'] );

            if( isset($data['status']) )
                $query->where( 'status', $data['status'] );

            if( isset($data['veiculo_id']) )
                $query->where( 'veiculo_id', $data['veiculo_id'] );

            if( isset($data['dt_inicio']) )
                $query->where( 'dt_inicio', $data['dt_inicio'] );
            
            if( isset($data['dt_fim']) )
                $query->where( 'dt_fim', $data['dt_fim'] );
    
        })->paginate($total);

    }

    public function saveInitLavajato( $data )
    {
        try {
            DB::beginTransaction();
            
            if( Arr::has($data, 'veiculo_id') ){
                $veiculo = Veiculo::find($data['veiculo_id']);
            }else{                
                $veiculo = Veiculo::create($data['veiculo']);
            }            

            $temp = $this->create([
                 'veiculo_id' => $veiculo->id                
            ]);

            $temp->veiculoEntradaSaida()->create([     
                //'veiculo_lava_jato_id' => $temp->id,
                'tipo' => 'E'
            ]);

            DB::commit();

            return $temp;

        } catch (\PDOException $e) {
            
            DB::rollBack();

            return $e;
        }
    }
}
