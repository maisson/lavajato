<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeiculoEntraSaida extends Model
{
    
    protected $fillable = ['veiculo_lava_jato_id', 'tipo', 'km', 'combustivel', 'dt_entradasaida']; 

    public function veiculo()
    {
        return $this->belongsTo(VeiculoLavaJato::class);
    }

    public function getResults( $data, $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['veiculo_lava_jato_id']) )
                $query->where( 'veiculo_lava_jato_id', $data['veiculo_lava_jato_id'] );
    
            if( isset($data['tipo']) )
                $query->where( 'tipo', $data['tipo']);

            if( isset($data['dt_entradasaida']) )
                $query->where( 'dt_entradasaida', $data['dt_entradasaida']);

        })->paginate($total);

    }

    public function getVeiculosE( $data1,  $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['veiculo_lava_jato_id']) )
                $query->where( 'veiculo_lava_jato_id', $data['veiculo_lava_jato_id'] );
    
            if( isset($data['tipo']) )
                $query->where( 'tipo', $data['tipo']);

            if( isset($data['dt_entradasaida']) )
                $query->where( 'dt_entradasaida', $data['dt_entradasaida']);

        })->paginate($total);

    }


}
