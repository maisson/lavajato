<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeiculoServico extends Model
{
    protected $fillable = ['id','veiculo_lava_jato_id','funcionario_id', 'servico_id', 'observacoes', 'dt_inicio', 'dt_fim']; 
    
    public function getResults( $data, $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['id']) )
                $query->where( 'id', $data['id'] );

            if( isset($data['veiculo_lava_jato_id']) )
                $query->where( 'veiculo_lava_jato_id', $data['veiculo_lava_jato_id'] );
    
        })->paginate($total);

    }
}
