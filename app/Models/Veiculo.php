<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $fillable = ['id','placa', 'cor', 'modelo', 'marca']; 

    public function veiculo_lavajato()
    {
        return $this->hasMany(VeiculoLavaJato::class);
    }
    
    public function getResults( $data, $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['id']) )
                $query->where( 'id', $data['id'] );

            if( isset($data['placa']) )
                $query->where( 'placa', $data['placa'] );
    
        })->paginate($total);

    }

}
