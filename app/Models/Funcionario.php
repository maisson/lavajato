<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $fillable = ['id','nome']; 
    
    public function getResults( $data, $total )
    {
        return $this->where( function($query) use ($data) {

            if( isset($data['id']) )
                $query->where( 'id', $data['id'] );

            if( isset($data['nome']) )
                $query->where( 'nome', $data['nome'] );
    
        })->paginate($total);

    }
}
