<?php

namespace App\Http\Controllers\Auth\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use App\Http\Controllers\Auth\Api\Traits\AuthTrait;

use App\Http\Requests\UserRequest;

class ProfileApiController extends Controller
{
    use AuthTrait;


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['register']]);
    }


    public function register(UserRequest $request, User $user)
    {
        $data = $request->only(['name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);
        $user->create($data);

        return $this->authenticate();
    }


    public function update(UserRequest $request)
    {
        $response = $this->getUser();
        if ($response['status'] != 200)
            return response()->json([$response['response']], $response['status']);
        
        $user = $response['response'];

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user->update($data);

        return response()->json(compact('user'));
    }
}
