<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Servico;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicoController extends Controller
{
    private $record;

    public function __construct(  Servico $servico ) {
        $this->record = $servico;
    }

    public function index(Request $request)
    {
        $data = $this->record->getResults( $request->all(), 2 );

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $data = $this->record->create( $request->all() );

        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        return response()->json($data, 200);

    }

    public function update(Request $request, $id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->update($request->all());

        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->delete();

        return response()->json(['success' => true], 204);
    }
}
