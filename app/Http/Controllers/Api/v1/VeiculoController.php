<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Veiculo;

use App\Http\Requests\VeiculoRequest;



class VeiculoController extends Controller
{
    private $record;

    public function __construct(  Veiculo $veiculo ) {
        $this->record = $veiculo;
    }

    public function index(Request $request)
    {
        $data = $this->record->getResults( $request->all(), 20 );

        return response()->json($data, 200);
    }

    public function store(VeiculoRequest $request)
    {
        $data = $this->record->create( $request->all() );

        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        return response()->json($data, 200);

    }

    public function update(VeiculoRequest $request, $id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->update($request->all());

        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->delete();

        return response()->json(['success' => true], 204);
    }
}
