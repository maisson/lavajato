<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\VeiculoEntraSaida;

class VeiculoEntradaSaidaController extends Controller
{
    private $record; 
    private $totalPage = 10;

    public function __construct(  VeiculoEntraSaida $veiculoEntraSaida ) {
        $this->record = $veiculoEntraSaida;
    }

    public function index(Request $request)
    {
        $data = $this->record->getResults( $request->all(), 2 );

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $data = $this->record->create( $request->all() );

        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        return response()->json($data, 200);

    }

    public function update(Request $request, $id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->update($request->all());

        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $data = $this->record->find($id);

        if(!$data)
            return response()->json(['error'=>'Not found'], 404);

        $data->delete();

        return response()->json(['success' => true], 204);
    }
}
