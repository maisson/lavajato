<?php

/*Route::get('veiculos', 'Api\VeiculoController@index');
Route::post('veiculos', 'Api\VeiculoController@store');
Route::put('veiculos/{id}', 'Api\VeiculoController@update');
Route::delete('veiculos/{id}', 'Api\VeiculoController@destroy');*/

Route::get('me', 'Auth\AuthApiController@getAuthenticatedUser');


Route::group(['namespace' => 'Auth\Api'], function () {

    Route::post('auth', 'AuthApiController@authenticate');    
    
    Route::get('me2', 'AuthApiController@getAuthenticatedUser');

    Route::post('register', 'ProfileApiController@register');
    Route::put('update', 'ProfileApiController@update');

});


Route::group(['prefix' => 'v1', 
              'namespace' => 'Api\v1',
              'middleware' => 'jwt.auth'

], function() {

    Route::apiResource('veiculos', 'VeiculoController');
    Route::apiResource('veiculosentradasaida', 'VeiculoEntradaSaidaController');

    Route::apiResource('servico', 'ServicoController');
    Route::apiResource('funcionario', 'FuncionarioController');

    Route::apiResource('lavajato', 'VeiculoLavaJatoController');
    Route::post('lavajato/storeAll', 'VeiculoLavaJatoController@storeAll');

});

