import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '../vuex/store';

import AdminComponent from '../components/admin/AdminComponent';

import VeiculoListComponent from '../components/admin/pages/veiculo/VeiculoListComponent';
import VeiculoFormComponent from '../components/admin/pages/veiculo/VeiculoFormComponent';

import ServicoListComponent from '../components/admin/pages/servico/ServicoListComponent';
import ServicoFormComponent from '../components/admin/pages/servico/ServicoFormComponent';

import FuncionarioListComponent from '../components/admin/pages/funcionario/FuncionarioListComponent';
import FuncionarioFormComponent from '../components/admin/pages/funcionario/FuncionarioFormComponent';

import DashboardComponent from '../components/admin/pages/dashboard/DashboardComponent';
import LavajatoComponent from '../components/admin/pages/lavajato/LavajatoComponent';

import LoginComponent from '../components/admin/LoginComponent';

Vue.use(VueRouter);

const routes = [
    { 
        path: '/admin', 
        component: AdminComponent,
        meta:{auth: true},
        children:[
            { path: '', component: DashboardComponent, name: 'admin.dashboard' },
            
            { path: 'veiculo', component: VeiculoListComponent, name: 'admin.veiculo.list' },   
            { path: 'veiculo/form', component: VeiculoFormComponent, name: 'admin.veiculo.form' },   
            { path: 'veiculo/form/:id', component: VeiculoFormComponent, name: 'admin.veiculo.edit', props:true },  
            
            { path: 'servico', component: ServicoListComponent, name: 'admin.servico.list' },   
            { path: 'servico/form', component: ServicoFormComponent, name: 'admin.servico.form' },   
            { path: 'servico/form/:id', component: ServicoFormComponent, name: 'admin.servico.edit', props:true },  

            { path: 'funcionario', component: FuncionarioListComponent, name: 'admin.funcionario.list' },   
            { path: 'funcionario/form', component: FuncionarioFormComponent, name: 'admin.funcionario.form' },   
            { path: 'funcionario/form/:id', component: FuncionarioFormComponent, name: 'admin.funcionario.edit', props:true }, 

            { path: 'lavajato', component: LavajatoComponent, name: 'admin.lavajato' },   
        ],        

        
    },
    
    {path: '/login', component:LoginComponent,  name:'login'},

    //{path: '*', redirect:{name:'admin.veiculo.list'}}


];

const router = new VueRouter({
    mode: 'history',
    routes
})


//Verificar se está logado no sistema
router.beforeEach((to, from, next) =>{

    if(to.meta.auth && !store.state.login.authenticated){   
        store.commit('CHANGE_URL_BACK', to.name);
        return router.push({name: 'login'})
    }

    if(to.matched.some(record => record.meta.auth) && !store.state.login.authenticated){
        store.commit('CHANGE_URL_BACK', to.name);
        return router.push({name: 'login'}) 
    }

    next();

});

export default router;