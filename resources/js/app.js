require('./bootstrap');

window.Vue = require('vue');


import router from './routes/routers';
import store from './vuex/store';
import Toasted from 'vue-toasted';
import VueSelect from 'vue-cool-select'

Vue.use(Toasted, { theme: "toasted-primary", position: "top-center", duration : 5000 });

Vue.component( 'admin-component', require('./components/admin/AdminComponent').default );
Vue.component( 'preloader-component', require('./components/layouts/PreloaderComponent').default );



Vue.use(VueSelect, {
  theme: 'material-design'
})

const app = new Vue({  
    router,
    store,      
    Toasted,
    el: '#app',    
});

store.dispatch('checkLogin').then(()=>{      
    router.push({name: store.state.login.urlBack });
    //store.commit('CHANGE_VISIBIBLE', true)   
    store.commit('PRELOADER', false)      
}).catch( err =>{ 
    if(err.status == 401)
        store.commit('CHANGE_VISIBIBLE', true) 
});

