import Vue from 'vue';
import Vuex from 'vuex';

import veiculos from './modules/veiculo/veiculo'
import preloader from './modules/preloader/preloader'

import login from './modules/login/login'
import funcionario from './modules/funcionario/funcionario'
import servico from './modules/servico/servico'
import lavajato from './modules/lavajato/lavajato'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        veiculos: veiculos,
        preloader: preloader,
        login:login,
        servico:servico,
        funcionario:funcionario,
        lavajato:lavajato,
    }
})

export default store;