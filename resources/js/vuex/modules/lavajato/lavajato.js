import {URL_BASE} from '../../../config/config';

const RESOURCE = 'lavajato';

export default {
    state:{
        items: {
            data:[]
        },
    },
    mutations:{
        LOAD_LAVAJATO(state, values){
            state.items = values
        }
    },
    actions:{       
        loadLavaJato(context, params){

            context.commit('PRELOADER', true)

            return new Promise((resolve, reject) => {

            axios.get(`${URL_BASE}${RESOURCE}`, {params})
            .then(res =>{                                
                context.commit('LOAD_LAVAJATO', res);  
                resolve();                             
            })
            .catch( err => reject(err) )
            .finally(() => context.commit('PRELOADER', false) );

            })
            
        },
        loadOneLavaJato(context, id){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {

                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                .then( res => resolve( res.data ) )            
                .catch( err => reject( err ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
            
        },
        storeLavaJato(context, params){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {

                axios.post(`${URL_BASE}${RESOURCE}`, params )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
        },
        storeLavaJatoAll(context, params){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {

                axios.post(`${URL_BASE}${RESOURCE}/storeAll`, params )
                .then( res => resolve(res) )            
                .catch( err => reject( err.response.data.errors ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
        },
        updateLavaJato(context, params){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {                

                axios.put(`${URL_BASE}${RESOURCE}/${params.id}`, params )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
        },
        destroyLavaJato(context, id){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {                

                axios.delete(`${URL_BASE}${RESOURCE}/${id}` )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                //.finally(() => context.commit('PRELOADER', false) );

            })
        },

    },
    getters:{
        
    }
}