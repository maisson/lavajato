import {TOKEN_NAME} from '../../../config/config';


const URL_BASE = '/api/';
const RESOURCE = 'auth';

export default {
    state:{
        user: {},
        authenticated: false,
        urlBack: 'admin.dashboard',
        isVisibleLogin: false,
    },
    mutations:{
        AUTH_USER_OK(state, values){
            state.user = values;
            state.authenticated = true;
        },

        CHANGE_URL_BACK(state, url){
            state.urlBack = url;
        },

        CHANGE_VISIBIBLE(state, value){
            state.isVisibleLogin = value;
        }
    },
    actions:{       
        login(context, params){
            
            context.commit('PRELOADER', true)

            return axios.post(`${URL_BASE}${RESOURCE}`, params)
            .then(res =>{                                                                         
                context.commit('AUTH_USER_OK', res.data.user);                                                  
                
                let token = res.data.token

                localStorage.setItem(TOKEN_NAME, token);
                window.axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                
            }).catch( () =>{ reject() })
            .finally(() => context.commit('PRELOADER', false) );
            
        },
        checkLogin(context){

            return new Promise( (resolve, reject) =>{

                const token = localStorage.getItem(TOKEN_NAME);
                if(!token)
                        return resolve();

                axios.get(`${URL_BASE}me`)
                .then(res =>{                                                                         
                    context.commit('AUTH_USER_OK', res.data.user);  
                    //localStorage.setItem(TOKEN_NAME, res.data.token)                                                                                                        
                    resolve()
                })
                .catch( err => reject(err.response) )
                .finally(() => context.commit('PRELOADER', false) );  
                
            })


        }

    },
    getters:{
        
    }
}