import {URL_BASE} from '../../../config/config';

const RESOURCE = 'veiculos';

export default {
    state:{
        items: {
            data:[]
        },
    },
    mutations:{
        LOAD_VEICULO(state, values){
            state.items = values
        }
    },
    actions:{       
        loadVeiculo(context, params){

            context.commit('PRELOADER', true)

            return new Promise((resolve, reject) => {

            axios.get(`${URL_BASE}${RESOURCE}`, {params})
            .then(res =>{                                
                context.commit('LOAD_VEICULO', res);  
                resolve(res.data);                             
            })
            .catch( err => reject(err) )
            .finally(() => context.commit('PRELOADER', false) );

            })
            
        },
        loadOneVeiculo(context, id){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {

                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                .then( res => resolve( res.data ) )            
                .catch( err => reject( err ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
            
        },
        storeVeiculo(context, params){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {

                axios.post(`${URL_BASE}${RESOURCE}`, params )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
        },
        updateVeiculo(context, params){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {                

                axios.put(`${URL_BASE}${RESOURCE}/${params.id}`, params )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                .finally(() => context.commit('PRELOADER', false) );

            })
        },
        destroyVeiculo(context, id){

            context.commit('PRELOADER', true)
            
            return new Promise((resolve, reject) => {                

                axios.delete(`${URL_BASE}${RESOURCE}/${id}` )
                .then( res => resolve() )            
                .catch( err => reject( err.response.data.errors ) )
                //.finally(() => context.commit('PRELOADER', false) );

            })
        },

    },
    getters:{
        
    }
}