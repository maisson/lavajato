<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lavajato</title>
        <meta name="csrf-token" content="{{ csrf_token() }}"> 
        
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <link rel="stylesheet" href="css/all-themes.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/adminbsb.css">
        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
        

    </head>
    <body>
        <div id="app" >
            <vue-snotify></vue-snotify>
            <preloader-component></preloader-component>
            <router-view></router-view>

        </div>

        
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="js/template.js"></script>


    </body>
</html>
